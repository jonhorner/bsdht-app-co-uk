#  - README

Repo for  website.

Git repo:
https://aspire.endocom.net:4434/aspire/bsdht-app-co-uk

Basecamp project:
https://basecamp.com/2599772/projects/

Website:
[http://www.bsdht-app.co.uk](http://www.bsdht-app.co.uk)

## Setup

```
cd [into repo]
npm install
```

## Development

Directory setup:

```
│
├── assets - compiled CSS, JS, for syncing with server
└── assets/js
└── assets/css
└── assets/images
└── assets/fonts
└── build - LESS files, original images,etc.
└── build/styles
└── build/scripts
└── build/images
└── build/fonts
```

Getting started:

```
subl .
gulp watch
```

for live sites
```
subl .
gulp watchlive
```

Watch should sync the files automatically, but if you want to force a sync:

```
gulp sass-deploy
gulp sass-deploy-dev
gulp scripts-deploy
gulp scripts-deploy-dev
```

For legacy sites use:

```
gulp styles-deploy
gulp styles-deploy-dev
gulp scripts-deploy
gulp scripts-deploy-dev
```

## Technology

* gulp
* SASS

