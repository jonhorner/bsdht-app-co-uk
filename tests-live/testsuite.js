var screenshotUrl = 'http://www.bsdht-app.co.uk',
    screenshotNow = new Date(),
    screenshotDateTime = screenshotNow.getFullYear() + pad(screenshotNow.getMonth() + 1) + pad(screenshotNow.getDate()) + '-' + pad(screenshotNow.getHours()) + pad(screenshotNow.getMinutes()) + pad(screenshotNow.getSeconds());
    pages = [
      { 'title': 'home' , 'url': screenshotUrl },
      { 'title': 'about-us' , 'url' : screenshotUrl+'about-us' }
    ];

casper.start(screenshotUrl, function() {
 this.echo('Casper started', 'info');
});

casper.each(pages, function(casper, page) {
  //this.echo('Current location is ' + page.url, 'info');

  this.thenOpen(page.url, function() {
    this.click('.cc-cookie-accept');
    this.wait(50);
  });


  this.then(function(){
    phantomcss.screenshot('body', args.viewportSize[0]+'/'+args.viewportSize[0]+'-'+page.title);
  }); 


});

casper.run(function() {
    this.echo('Finished captures for ' + screenshotUrl).exit();
});

//casper.run();

function pad(number) {
  var r = String(number);
  if ( r.length === 1 ) {
    r = '0' + r;
  }
  return r;
}