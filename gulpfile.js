// ## Globals
/*global $:true*/
var $           = require('gulp-load-plugins')(),
    argv        = require('yargs').argv,
    gulp        = require('gulp'),
    lazypipe    = require('lazypipe'),
    merge       = require('merge-stream'),
    jshint      = require('gulp-jshint'),
    uglify      = require('gulp-uglify'),
    requireDir  = require('require-dir'),
    Path        = require('path');

if(global.process.env.LOGNAME==='max'){
  console.log('Notifications disabled');
  process.env.DISABLE_NOTIFIER=true;
}

// See https://github.com/austinpray/asset-builder
var manifest = require('asset-builder')('./build/manifest.json');

// `path` - Paths to base asset directories. With trailing slashes.
// - `path.source` - Path to the source files. Default: `assets/`
// - `path.dist` - Path to the build directory. Default: `dist/`
var path = manifest.paths;

// `config` - Store arbitrary configuration values here.
var config = manifest.config || {};

// `globs` - These ultimately end up in their respective `gulp.src`.
// - `globs.js` - Array of asset-builder JS dependency objects. Example:
//   ```
//   {type: 'js', name: 'main.js', globs: []}
//   ```
// - `globs.css` - Array of asset-builder CSS dependency objects. Example:
//   ```
//   {type: 'css', name: 'main.css', globs: []}
//   ```
// - `globs.fonts` - Array of font path globs.
// - `globs.images` - Array of image path globs.
// - `globs.bower` - Array of all the main Bower files.
var globs = manifest.globs;

// `project` - paths to first-party assets.
// - `project.js` - Array of first-party JS assets.
// - `project.css` - Array of first-party CSS assets.
var project = manifest.getProjectGlobs();

// CLI options
var enabled = {
  // Enable static asset revisioning when `--production`
  rev: argv.production,
  // Disable source maps when `--production`
  maps: !argv.production,
  // Fail styles task on error when `--production`
  failStyleTask: argv.production
};

// Path to the compiled assets manifest in the dist directory
var revManifest = path.dist + 'assets.json';


// ### Write to rev manifest
// If there are any revved files then write them to the rev manifest.
// See https://github.com/sindresorhus/gulp-rev
var writeToManifest = function(directory) {
  return lazypipe()
    .pipe(gulp.dest, path.dist + directory)
    .pipe(function() {
      return $.if('**/*.{js,css}', browserSync.reload({stream:true}));
    })
    .pipe($.rev.manifest, revManifest, {
      base: path.dist,
      merge: true
    })
    .pipe(gulp.dest, path.dist)();
};








gulp.task('watch', function() {
  gulp.watch([path.source + '/styles/**/*'], ['sass-deploy-dev']);
  gulp.watch([path.source + '/js/**/*'], ['scripts-deploy-dev']);

  
});

gulp.task('watchlive', function() {
  gulp.watch([path.source + '/styles/**/*'], ['sass-deploy']);
  gulp.watch([path.source + '/js/**/*'], ['scripts-deploy']);
});


// Import all tasks
var tasks = requireDir(global.process.env.HOME+'/Sites/aspire-gulp-tasks', {}, [$, gulp, config, path]);
